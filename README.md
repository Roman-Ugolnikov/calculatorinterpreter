### What is this repository for? ###

Implementation of "Interpreter" pattern. Calculator that can accept formula like

* 8 * (4 + 3) - 2 / pi
* 8 TIMES (4 PLUS 3) MINUS 2 DIVIDE PI

### How to run? ###

It is java application that will be packaged into runnable jar file. It uses JavaFX for UI.
To package it run:

```
./gradlew build
```

and then run jar

to run it from gradle : 

```
./gradlew run
```
 