package calculator;


import calculator.interpreter.AbstractExpression;
import calculator.interpreter.Expression;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * @author Roman Uholnikov
 */
public class Main extends Application {

    @FXML
    private Button calculateButton;

    @FXML
    private Label resultLabel;

    @FXML
    private TextField expressionFiled;

    @FXML
    private RadioButton codeRadioButton;

    @FXML
    private RadioButton codeEnRadioButton;

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource("sample.fxml")
        );
        loader.setController(this);
        Parent root = loader.load();
        primaryStage.setTitle("Calculator.");
        primaryStage.setScene(new Scene(root, 600, 350));


        calculateButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Expression expression = new AbstractExpression(expressionFiled.getText());
                double result = expression.interpret();
                resultLabel.setText(String.valueOf(result));
            }
        });

        codeRadioButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                expressionFiled.setPromptText("enter expression like: 8 * (4 + 3) - 2 / pi");
                AbstractExpression.setStandardLanguageDictionary();
            }
        });

        codeEnRadioButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                expressionFiled.setPromptText("enter expression like: 8 TIMES (4 PLUS 3) MINUS 2 DIVIDE PI");
                AbstractExpression.setEnglishLanguageDictionary();
            }
        });



        primaryStage.getIcons().add(new Image("https://bitbucket.org/Roman-Ugolnikov/language-helper/downloads/piledbooks.png"));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }


}
