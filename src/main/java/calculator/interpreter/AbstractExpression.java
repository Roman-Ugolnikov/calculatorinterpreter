package calculator.interpreter;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Roman Uholnikov
 */
public class AbstractExpression implements Expression{

    String stringExpression;

    /**
     * list of available in our language commands
     * It is mapped to class names.
     */
    public static Map<String, String> mapOfLanguageDictionary;

    static{
        setStandardLanguageDictionary();
    }

    public AbstractExpression(final String stringExpression) {
        String expression = stringExpression.trim();
        if(expression.startsWith("(") && expression.endsWith(")")){
            this.stringExpression = expression.substring(1, expression.length() - 1);
        } else {
            this.stringExpression = stringExpression;
        }
    }

    public static void setStandardLanguageDictionary(){
        mapOfLanguageDictionary = new HashMap<>();
        mapOfLanguageDictionary.put("calculator.interpreter.Number", "pi");
        mapOfLanguageDictionary.put("calculator.interpreter.Plus", "+");
        mapOfLanguageDictionary.put("calculator.interpreter.Minus", "-");
        mapOfLanguageDictionary.put("calculator.interpreter.Times", "*");
        mapOfLanguageDictionary.put("calculator.interpreter.Divide", "/");
    }

    public static void setEnglishLanguageDictionary(){
        mapOfLanguageDictionary = new HashMap<>();
        mapOfLanguageDictionary.put("calculator.interpreter.Number", "PI");
        mapOfLanguageDictionary.put("calculator.interpreter.Plus", "PLUS");
        mapOfLanguageDictionary.put("calculator.interpreter.Minus", "MINUS");
        mapOfLanguageDictionary.put("calculator.interpreter.Times", "TIMES");
        mapOfLanguageDictionary.put("calculator.interpreter.Divide", "DIVIDE");

    }

    @Override
    public double interpret() {

        Expression plusOrMinusExpression = getMathOperationExpression(stringExpression, new Plus(), new Minus());
        if(plusOrMinusExpression != null){
            return plusOrMinusExpression.interpret();
        }

        Expression timesOrDivExpression = getMathOperationExpression(stringExpression, new Times(), new Divide());
        if(timesOrDivExpression != null){
            return timesOrDivExpression.interpret();
        }

        Expression numberExpression = getNumberExpression(stringExpression);
        if(numberExpression != null){
            return numberExpression.interpret();
        }

        return 0;
    }

    private Expression getNumberExpression(String stringExpression) {
        try{
            double v = Double.parseDouble(stringExpression.trim());
            return new Number(v);
        } catch (NumberFormatException e) {
            //do nothing
        }

        if(stringExpression.equalsIgnoreCase(mapOfLanguageDictionary.get(Number.class.getName()))){
            return new Number(Math.PI);
        }
        //here could be other constants
        return null;

    }

    /**
     * Tries to interpret expression as basic math operation sum minus times divide:
     * <p> e.g </p>
     * {@code}expression + expression{@code}
     *
     * @param expression string expression
     * @param expression1 instance of expression with particular class that will be tried to be created.
     * @param expression2 instance of expression with particular class that will be tried to be created.
     * @return Expression ready to interpret
     */
    private Expression getMathOperationExpression(String expression,
                                                  BaseMathExpression expression1,
                                                  BaseMathExpression expression2){
        String symbol1 = mapOfLanguageDictionary.get(expression1.getClass().getName());
        String symbol2 = mapOfLanguageDictionary.get(expression2.getClass().getName());
        final int openParentheses = stringExpression.indexOf("(");
        final int closeParentheses = stringExpression.indexOf(")", openParentheses);
        int indexOfFirst;
        int indexOfSecond;
        if(closeParentheses > 0){ //we have parentheses here
            //try to get operation before parentheses
            if(StringUtils.indexOf(expression.substring(0, openParentheses), symbol1) > 0){
                indexOfFirst =StringUtils.indexOf(expression.substring(0, openParentheses), symbol1);
                indexOfSecond =StringUtils.indexOf(expression.substring(0, openParentheses), symbol2);
            } else { //try to get operation after parentheses
                indexOfFirst = StringUtils.indexOf(expression, symbol1, closeParentheses);
                indexOfSecond = StringUtils.indexOf(expression, symbol2, closeParentheses);
            }
        } else { //we do not have parentheses here
            indexOfFirst = StringUtils.indexOf(expression, symbol1);
            indexOfSecond = StringUtils.indexOf(expression, symbol2);
        }
        if(indexOfSecond < 0 && indexOfFirst < 0){
            return null;
        }
        if(indexOfSecond < indexOfFirst){
            String leftExpression = StringUtils.substringBefore(expression, symbol1);
            String rightExpression = StringUtils.substringAfter(expression, symbol1);
            expression1.setLeftOperand(new AbstractExpression(leftExpression));
            expression1.setRightOperand(new AbstractExpression(rightExpression));
            return expression1;
        }else{
            String leftExpression = StringUtils.substringBefore(expression, symbol2);
            String rightExpression = StringUtils.substringAfter(expression, symbol2);
            expression2.setLeftOperand(new AbstractExpression(leftExpression));
            expression2.setRightOperand(new AbstractExpression(rightExpression));
            return expression2;
        }

    }
}
