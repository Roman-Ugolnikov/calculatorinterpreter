package calculator.interpreter;

/**
 * Interface for basic math operation.
 * In terms of Interpreter design pattern it is NotTerminatedExpression.
 */
public interface BaseMathExpression extends Expression{

    void setLeftOperand(Expression leftOperand);

    void setRightOperand(Expression rightOperand);
}
