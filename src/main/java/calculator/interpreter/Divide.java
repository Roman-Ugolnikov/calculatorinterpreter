package calculator.interpreter;

/**
 * @author Roman Uholnikov
 */
public class Divide implements BaseMathExpression {
    Expression leftOperand;
    Expression rightOperand;

    public Divide() {
    }

    public Divide(Expression left, Expression right) {
        leftOperand = left;
        rightOperand = right;
    }

    public double interpret() {
        return leftOperand.interpret() / rightOperand.interpret();
    }

    public void setLeftOperand(Expression leftOperand) {
        this.leftOperand = leftOperand;
    }

    public void setRightOperand(Expression rightOperand) {
        this.rightOperand = rightOperand;
    }
}
