package calculator.interpreter;

/**
 * Interface Interpreter.
 *
 * @author Roman Uholnikov
 */
public interface Expression {

    double interpret();
}
