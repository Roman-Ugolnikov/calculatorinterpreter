package calculator.interpreter;

/**
 * In terms of Interpreter design pattern it is TerminatedExpression.
 * @author Roman Uholnikov
 */
public class Number implements Expression {
    private double number;

    public Number(double number) {
        this.number = number;
    }

    public double interpret() {
        return number;
    }
}
