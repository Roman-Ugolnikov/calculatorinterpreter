package calculator.interpreter;

/**
 * @author Roman Uholnikov
 */
public class Plus implements BaseMathExpression {
    Expression leftOperand;
    Expression rightOperand;

    public Plus() {
    }

    public Plus(Expression left, Expression right) {
        leftOperand = left;
        rightOperand = right;
    }

    public double interpret() {
        return leftOperand.interpret() + rightOperand.interpret();
    }

    public Expression getLeftOperand() {
        return leftOperand;
    }

    public void setLeftOperand(Expression leftOperand) {
        this.leftOperand = leftOperand;
    }

    public Expression getRightOperand() {
        return rightOperand;
    }

    public void setRightOperand(Expression rightOperand) {
        this.rightOperand = rightOperand;
    }
}