package calculator.interpreter;

/**
 * @author Roman Uholnikov
 */
public class Times implements BaseMathExpression {
    Expression leftOperand;
    Expression rightOperand;

    public Times() {
    }

    public Times(Expression left, Expression right) {
        leftOperand = left;
        rightOperand = right;
    }

    public double interpret() {
        return leftOperand.interpret() * rightOperand.interpret();
    }

    public void setLeftOperand(Expression leftOperand) {
        this.leftOperand = leftOperand;
    }

    public void setRightOperand(Expression rightOperand) {
        this.rightOperand = rightOperand;
    }
}
